FROM fabric8/java-alpine-openjdk11-jre AS build-env

COPY target/lib/* /deployments/lib/

COPY target/*-runner.jar /deployments/app.jar

FROM gcr.io/distroless/java:11

ENV AB_ENABLED=jmx_exporter

COPY --from=build-env /deployments /app

ENV JAVA_TOOL_OPTIONS="-Dquarkus.http.host=0.0.0.0 -Djava.util.logging.manager=org.jboss.logmanager.LogManager -XX:+UseContainerSupport -XX:ReservedCodeCacheSize=20m -Xms51m -Xmx62m -Xss256k -XX:MetaspaceSize=71m -XX:MaxMetaspaceSize=75m -Xmn17m -XX:MaxDirectMemorySize=8m -XX:MaxGCPauseMillis=400 -Dfile.encoding=UTF-8 -Djava.net.preferIPv4Stack=true -XX:+UseG1GC -XX:+UseStringDeduplication -XX:InitiatingHeapOccupancyPercent=70 -XX:ConcGCThreads=1 -XX:ParallelGCThreads=1 -Dsun.net.inetaddr.ttl=600 -XX:+DisableAttachMechanism -XX:+OptimizeStringConcat"

WORKDIR /app

EXPOSE 8080

CMD ["app.jar"]
