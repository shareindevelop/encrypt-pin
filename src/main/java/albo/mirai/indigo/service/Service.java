package albo.mirai.indigo.service;

import albo.mirai.indigo.utils.EncryptPIN;

import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.logging.Logger;
import com.cavium.key.CaviumKey;


@ApplicationScoped
public class Service {

    private static final Logger LOG = Logger.getLogger("Service");

    @Inject
    EncryptPIN utilsCrypter;

    @ConfigProperty(name = "mirai.tutuka.hsm.key.handle")
    private long KEY_HANDLE;

    public String encrypt(String pin) throws Exception {

        String transformation = "DESede/ECB/Nopadding";
        CaviumKey secKey=null;
        String keyEncrypted="";
        try {
            secKey = utilsCrypter.getKeyByHandle(KEY_HANDLE);
        }catch (Exception e){
            throw e;
        }
        if(secKey==null)
            throw new Exception("secKey null");
        try {
            keyEncrypted = utilsCrypter.encode(pin, transformation, secKey);
        }catch (Exception e){
            throw new Exception("Exception Encode");
        }
        return keyEncrypted;
    }



    public String desencrypt(String pin) throws Exception {

        String transformation = "DESede/ECB/Nopadding";
        CaviumKey secKey=null;
        String keyEncrypted="";
        try {
            secKey = utilsCrypter.getKeyByHandle(KEY_HANDLE);
        }catch (Exception e){
            throw e;
        }
        if(secKey==null)
            throw new Exception("secKey null");
        try {
            keyEncrypted = utilsCrypter.decode(pin, transformation, secKey);
        }catch (Exception e){
            throw new Exception("Exception Encode");
        }
        return keyEncrypted;
    }
}
