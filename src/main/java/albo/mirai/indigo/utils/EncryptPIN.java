package albo.mirai.indigo.utils;

import com.cavium.cfm2.CFM2Exception;
import com.cavium.cfm2.Util;
import com.cavium.key.CaviumDES3Key;
import com.cavium.key.CaviumKey;
import com.cavium.key.CaviumKeyAttributes;
import org.apache.commons.codec.binary.Hex;

import javax.crypto.Cipher;
import javax.enterprise.context.ApplicationScoped;
import java.io.IOException;
import java.security.Key;
import java.security.Security;
import java.util.logging.Level;
import java.util.logging.Logger;

@ApplicationScoped
public class EncryptPIN {

    private static final Logger LOG = Logger.getLogger("EncryptPIN");

    private String formatPinBlock(String pin, int format) throws Exception {
        StringBuilder formattedPin = new StringBuilder();
        String dpb;
        byte[] anbValue;
        byte[] pinValue;
        byte[] dpbValue = new byte[8];
        int i = 0;
        formattedPin.append(format);
        formattedPin.append(pin.length());
        formattedPin.append(pin);
        int count = 14 - pin.length();
        formattedPin.append(format == 0 ? new String(new char[count]).replace("\0", "F") : new String(new char[count]).replace("\0", "0"));
        pinValue = Hex.decodeHex(formattedPin.toString().toCharArray());
        anbValue = Hex.decodeHex("0000000000000000".toCharArray());
        for (byte b : pinValue) {
            dpbValue[i] = (byte) (b ^ anbValue[i++]);
        }
        dpb = Hex.encodeHexString(dpbValue);
        return dpb;
    }
    private String getPinFromPinBlock(String dpb) throws Exception {
        byte[] dpbValue = Hex.decodeHex(dpb.toCharArray());
        byte[] anbValue = new byte[8];
        byte[] pinValue = new byte[8];
        int i = 0;
        anbValue = Hex.decodeHex("0000000000000000".toCharArray());
        for (byte b : dpbValue) {
            pinValue[i] = (byte) (b ^ anbValue[i++]);
        }
        String pinBlock = Hex.encodeHexString(pinValue);
        int len = Integer.parseInt(pinBlock.substring(1, 2)) + 2;
        String pin = pinBlock.substring(2, len);
        return pin;
    }
    public String encode(String pin, String transformation, Key secKey) throws Exception {
        byte[] dpbValue = Hex.decodeHex(formatPinBlock(pin,1).toCharArray());
        Cipher encrypter = Cipher.getInstance(transformation);
        encrypter.init(Cipher.ENCRYPT_MODE, secKey);
        byte[] encrypted = encrypter.doFinal(dpbValue);
        return Hex.encodeHexString(encrypted).toUpperCase();
    }


    public String decode(String epb, String transformation, Key secKey) throws Exception {
        byte[] epbValue = Hex.decodeHex(epb.toCharArray());
        Cipher decrypter = Cipher.getInstance(transformation);
        decrypter.init(Cipher.DECRYPT_MODE, secKey);
        byte[] decrypted = decrypter.doFinal(epbValue);
        return getPinFromPinBlock(Hex.encodeHexString(decrypted).toUpperCase());
    }


    public CaviumKey getKeyByHandle(long handle) throws CFM2Exception, IOException {

        // There is no direct method to load a key, but there is a method to load key attributes.
        // Using the key attributes and the handle, a new CaviumKey object can be created. This method shows
        // how to create a specific key type based on the attributes.
        try {
            Security.addProvider(new com.cavium.provider.CaviumProvider());
        } catch (IOException ex) {
            System.out.println(ex);
            throw ex;
        }



        byte[] keyAttribute = Util.getKeyAttributes(handle);
        CaviumKeyAttributes cka = new CaviumKeyAttributes(keyAttribute);

        if(cka.getKeyType() == CaviumKeyAttributes.KEY_TYPE_DES3){
            CaviumKey key = new CaviumDES3Key(handle, cka);
            return key;
        }


        return null;
    }
}