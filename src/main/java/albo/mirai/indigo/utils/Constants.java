package albo.mirai.indigo.utils;

public class Constants {


    private Constants(){}


    public static final String DOMAIN= "0012";
    public static final String SERVICE_DOMAIN= "019";

    public static final String JSON_DATA = "data";
    public static final String JSON_PAYLOAD_PIN = "pin";
}
